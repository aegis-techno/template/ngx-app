@echo off


set APP_NAME=ngx-app
set APP_DIRECTORY=ngx-app
set APP_PREFIX=app

ng new %APP_NAME% --package-manager=yarn --create-application=true --directory=%APP_DIRECTORY% --prefix=%APP_PREFIX% --strict --routing --style=scss || true

cd %APP_DIRECTORY% || exit

ng add @angular-eslint/schematics --skip-confirmation=true --defaults=true
ng add @cypress/schematic --skip-confirmation=true --defaults=true
ng add @angular/material@~12.2.0 --skip-confirmation=true --defaults=true
yarn add --dev sonarqube-scanner@2.8.1 karma-sonarqube-reporter@1.4.0

jq ".devDependencies += {\"cypress\": \"8.6.0\"}" package.json > package-tmp.json
move /Y package-tmp.json package.json
jq ".scripts += {\"test\": \"ng test --code-coverage\",\"sonar\":\"sonar-scanner\"}" package.json > package-tmp.json
move /Y package-tmp.json package.json
yarn