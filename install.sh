
APP_NAME="ngx-app"
APP_DIRECTORY="ngx-app"
APP_PREFIX="app"


ng new $APP_NAME --package-manager=yarn --create-application=true --directory=$APP_DIRECTORY --strict --routing --style=scss --prefix=$APP_PREFIX

cd $APP_DIRECTORY || exit

ng add @angular-eslint/schematics --skip-confirmation=true
ng add @cypress/schematic --skip-confirmation=true --defaults=true
ng add @angular/material@~12.2.0 --skip-confirmation=true --defaults=true
yarn add --dev sonarqube-scanner@2.8.1 karma-sonarqube-reporter@1.4.0


echo "$(jq '.devDependencies += {"cypress": "8.6.0"}' package.json)" > package.json
echo "$(jq '.scripts += {"test": "ng test --code-coverage","sonar": "sonar-scanner"}' package.json)" > package.json
yarn