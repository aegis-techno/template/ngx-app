# Angular application boilerplate

----------

This project describe the steps for produce a boilerplate for angular applications.

This boilerplate is configured for :

- Node lts/fermium
- Angular 12.2.11
- Es-Lint for linting
- Sonar 2.8.1 for analyse
- Karma for unit test
- Cypress 8.6.0 for test e2e
- Angular material 12.2.11 for design components

The result is display in directory `ngx-app`.

The scripts `install.(bat|sh)` launch a process with few question for recreate a new boilerplate.

## Prerequisite

The prerequisite are :

- Node lts/fermium
- The package manager yarn
- The package @angular/cli@12.2.11
- jq for manipulate json file

### Node

For install node.js the best is to use nvm. Nvm is a version manager for node.

#### Nvm installation

- For Window, nvm is available on https://github.com/coreybutler/nvm-windows
- For Unix, nvm is available on https://github.com/nvm-sh/nvm

#### Node installation

For old nvm version

```bash
nvm install 14.18.1
nvm alias default 14.18.1
nvm use 14.18.1
```

For newest version

```bash
nvm install lts/fermium
nvm alias default lts/fermium
nvm use lts/fermium
```

### Yarn

Yarn is more optimized, and stable than npm.

#### Yarn installation

For install yarn we need use npm

```bash
npm i -g yarn
```

And the path to yarn global binaries directory to your PATH for use global package installed

For linux, add in your `./bashrc` the following line for you yarn global binaries

```bash
export PATH="$PATH:$(yarn global bin)"
```

For window, add to your path the result of following command

```
yarn global bin
```

### Angular installation

For install Angular we use yarn with global option.

```bash
yarn global add @angular/cli@~12.2.0
```

### Jq

For linux, install jq package

```bash
sudo apt-get install jq.
```

For window, download jq executable on https://stedolan.github.io/jq/download/

## The boilerplate

The app is created with `ng new`, then cypress and es-lint schematics is added, and for finish material,sonar and a
reporter for karma

```bash
ng new ngx-app --package-manager=yarn --create-application=true --directory=ngx-app --strict --routing --style=scss

cd ngx-app

ng add @angular-eslint/schematics --skip-confirmation=true
ng add @cypress/schematic --skip-confirmation=true --defaults=true
ng add @angular/material@~12.2.0 --skip-confirmation=true --defaults=true
yarn add --dev sonarqube-scanner@2.8.1 karma-sonarqube-reporter@1.4.0
```

### Configuration

Many configuration is necessary :

- Package.json
- Karma
- Angular.json
- TsConfig
- Eslint

#### Package.json

We will fix cypress version and add a few scripts, after that we will recall yarn for reinstall cypress with fixed value

For linux :
```bash
echo "$(jq '.devDependencies += {"cypress": "8.6.0"}' package.json)" > package.json
echo "$(jq '.scripts += {"test": "ng test --code-coverage","sonar": "sonar-scanner"}' package.json)" > package.json
yarn
```

For windows :
```bash
jq ".devDependencies += {\"cypress\": \"8.6.0\"}" package.json > package-tmp.json
move /Y package-tmp.json package.json
jq ".scripts += {\"test\": \"ng test --code-coverage\",\"sonar\":\"sonar-scanner\"}" package.json > package-tmp.json
move /Y package-tmp.json package.json
yarn
```

#### Karma
#### Angular.json
#### tsconfig
#### eslint

## Usage

You can use the `script.(bat|sh)` for reinit the boilerplate with custom app name and prefix.

## Roadmap

- Configuration Karma
- Configuration angular.json
- Configuration tsconfig
- Configuration eslint
- Configuration editorConfig
